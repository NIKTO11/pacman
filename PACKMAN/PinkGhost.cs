﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    class PinkGhost : Ghost
    {
        public PinkGhost(Game game, Map map, int x, int y, int direction)
            : base(game, map, x, y, direction)
        {
            scaterx = 2;
            scatery = 20;
        }
        /// <summary>
        /// snaží sa dostať na najbližšiu križovatku pred pacmana
        /// </summary>
        public override void Chase()
        {
            int newx = 0;
            int newy = 0;
            int pdirection = 0;
            map.PacManPozition(ref newx, ref newy, ref pdirection);     //zisti pozíciu pacmana
            while (Corridor(newx, newy, pdirection))                    //pokiaľ nieje na križovatke
            {
                MoveInDirection(ref newx, ref newy, pdirection);        //posunie sa o políčko dopredu v smere pacmanových úst
            }
            Bfs(ref newx, ref newy);                                    //zistí políčko na ktoré sa má posunúť aby sa dotal na vybrané miesto
            Relocate(newx, newy);
        }
        /// <summary>
        /// udeká do ľavého horného rohu
        /// </summary>
        public override void Scater()
        {
            int newx = scaterx;
            int newy = scatery;
            Bfs(ref newx, ref newy);                                    //zistí políčko na ktoré sa má posunúť aby sa dotal na vybrané miesto
            Relocate(newx, newy);
        }
        /// <summary>
        /// pohybuje sa náhodne
        /// </summary>
        public override void Frightened()
        {
            int newx = 0;
            int newy = 0;
            Random(ref newx, ref newy);                                 //nahodná voľba pohybu
            Relocate(newx, newy);
        }

    }
}
