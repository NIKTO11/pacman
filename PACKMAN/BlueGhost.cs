﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    class BlueGhost : Ghost
    {
        public BlueGhost(Game game, Map map, int x, int y, int direction)
            : base(game, map, x, y, direction)
        {
            scaterx = 2;
            scatery = 2;
        }

        int moves;
        /// <summary>
        /// každých 10 sekund strieda prenasledovanie a a predbiehanie pacmana
        /// </summary>
        public override void Chase()
        {
            int newx = 0;
            int newy = 0;
            int pdirection = 0;
            map.PacManPozition(ref newx, ref newy, ref pdirection);         //zistí umiestnenie pacmana
            if (moves > 100)
            {
                while (Corridor(newx, newy, pdirection))
                {
                    MoveInDirection(ref newx, ref newy, pdirection);        //ak predbieha, najde najbližšiu križovatku pred pacmanom
                }
            }
            Bfs(ref newx, ref newy);                                        //zistí políčko na ktoré sa má posunúť aby sa dotal na vybrané miesto
            Relocate(newx, newy);                                           //posunie sa
            if (moves < 0)
                moves = 200;                                                //reštartovanie odpočtu krokov
        }
        /// <summary>
        /// uteká do ľavého horného rohu
        /// </summary>
        public override void Scater()
        {
            int newx = scaterx;
            int newy = scatery;
            Bfs(ref newx, ref newy);                                        //hladá najkratšiu cestu do ľavého horného rohu
            Relocate(newx, newy);
        }
        /// <summary>
        /// pohybuje sa náhodne
        /// </summary>
        public override void Frightened()
        {
            int newx = 0;
            int newy = 0;
            Random(ref newx, ref newy);                                     // náhnodne vyberie smer pohybu
            Relocate(newx, newy);
        }
    }
}
