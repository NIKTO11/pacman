﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/*
 * PACMAN
 * Martin Bakoš, 1.ročník
 * Letný semester 2018/2019
 * Programovanie II.
 */
namespace PACMAN
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Run(new Form1());   //zavolanie windowform
        }
    }
}
