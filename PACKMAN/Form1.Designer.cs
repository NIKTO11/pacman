﻿namespace PACMAN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelScoreValue = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelLives = new System.Windows.Forms.Label();
            this.labelLivesValue = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.labelCountdown = new System.Windows.Forms.Label();
            this.labelLevel = new System.Windows.Forms.Label();
            this.labelLevelValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.BackColor = System.Drawing.Color.Yellow;
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.startButton.Location = new System.Drawing.Point(400, 200);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(200, 50);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "START";
            this.startButton.UseVisualStyleBackColor = false;
            this.startButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Yellow;
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exitButton.Location = new System.Drawing.Point(400, 300);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(200, 50);
            this.exitButton.TabIndex = 1;
            this.exitButton.Text = "EXIT";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelScoreValue
            // 
            this.labelScoreValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScoreValue.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelScoreValue.Location = new System.Drawing.Point(840, 240);
            this.labelScoreValue.Name = "labelScoreValue";
            this.labelScoreValue.Size = new System.Drawing.Size(120, 40);
            this.labelScoreValue.TabIndex = 2;
            this.labelScoreValue.Text = "0";
            this.labelScoreValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelScoreValue.Visible = false;
            // 
            // labelScore
            // 
            this.labelScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScore.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelScore.Location = new System.Drawing.Point(840, 200);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(120, 40);
            this.labelScore.TabIndex = 3;
            this.labelScore.Text = "SCORE:";
            this.labelScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelScore.Visible = false;
            // 
            // labelLives
            // 
            this.labelLives.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLives.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelLives.Location = new System.Drawing.Point(840, 320);
            this.labelLives.Name = "labelLives";
            this.labelLives.Size = new System.Drawing.Size(120, 40);
            this.labelLives.TabIndex = 4;
            this.labelLives.Text = "LIVES:";
            this.labelLives.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLives.Visible = false;
            // 
            // labelLivesValue
            // 
            this.labelLivesValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLivesValue.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelLivesValue.Location = new System.Drawing.Point(840, 360);
            this.labelLivesValue.Name = "labelLivesValue";
            this.labelLivesValue.Size = new System.Drawing.Size(120, 40);
            this.labelLivesValue.TabIndex = 5;
            this.labelLivesValue.Text = "0";
            this.labelLivesValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelLivesValue.Visible = false;
            // 
            // labelName
            // 
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.ForeColor = System.Drawing.Color.Yellow;
            this.labelName.Location = new System.Drawing.Point(300, 50);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(400, 80);
            this.labelName.TabIndex = 6;
            this.labelName.Text = "PACMAN";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // labelCountdown
            // 
            this.labelCountdown.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCountdown.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelCountdown.Location = new System.Drawing.Point(320, 0);
            this.labelCountdown.Name = "labelCountdown";
            this.labelCountdown.Size = new System.Drawing.Size(200, 40);
            this.labelCountdown.TabIndex = 7;
            this.labelCountdown.Text = "READY!";
            this.labelCountdown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCountdown.Visible = false;
            // 
            // labelLevel
            // 
            this.labelLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevel.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelLevel.Location = new System.Drawing.Point(840, 80);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(120, 40);
            this.labelLevel.TabIndex = 8;
            this.labelLevel.Text = "LEVEL:";
            this.labelLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLevel.Visible = false;
            // 
            // labelLevelValue
            // 
            this.labelLevelValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevelValue.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelLevelValue.Location = new System.Drawing.Point(840, 120);
            this.labelLevelValue.Name = "labelLevelValue";
            this.labelLevelValue.Size = new System.Drawing.Size(120, 40);
            this.labelLevelValue.TabIndex = 9;
            this.labelLevelValue.Text = "0";
            this.labelLevelValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelLevelValue.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(984, 462);
            this.Controls.Add(this.labelLevelValue);
            this.Controls.Add(this.labelLevel);
            this.Controls.Add(this.labelCountdown);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelLivesValue);
            this.Controls.Add(this.labelLives);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.labelScoreValue);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.startButton);
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.Text = "PACMAN";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelScoreValue;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelLives;
        private System.Windows.Forms.Label labelLivesValue;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label labelCountdown;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label labelLevelValue;
    }
}