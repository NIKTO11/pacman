﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PACMAN
{
    /// <summary>
    /// obsahuje všetky informácie o hre a jej funkcie zabezpečujú fungovanie v jednotlivých stavoch
    /// </summary>
    class Game
    {
        Map map;                                                    //akualne hraná mapa
        string pathIcons;                                           //nazov súboru s ikonkami predmetov na mape
        string pathBonus;                                           //nazov súboru s ikonkami bonusu pre jednotlivé leveli
        string pathPlan;                                            //nazov súboru obsahujúci informácie o mape a jej pôdorys

        Graphics g;

        GameStatus gameStatus;                                      //stav hry
        int earnedScore;                                            //skore z predchádzajúcich levelov
        int lives;                                                  //počet zostávajúcich životov
        bool bonusLiveGiven;                                        //či bol udelený bonusový život
        int lvl;                                                    //level v ktorom sa hráč momentálne nachádza 
        string errMessage;                                          //popis chyby ktorá sa vyskytla

        public Game(string pathGame, Graphics g)
        {
            gameStatus = GameStatus.preparation;                    //hra sa pripravuje
            System.IO.StreamReader gameFile = new System.IO.StreamReader(pathGame); //otvorenie dokumentu s informáciami o hre
            pathPlan = gameFile.ReadLine();                         //prečítanie cesty k Planu
            pathIcons = gameFile.ReadLine();                        //prečitanie cesty k ikonam
            pathBonus = gameFile.ReadLine();                        //prečítanie cesty k ikonam bonusu
            gameFile.Close();                                       //ukončenie čítania

            this.g = g;                                             
            lvl = 1;                                                //začina sa na leveli 1
            earnedScore = 0;                                        //vynulovanie score
            lives = 3;                                              //začina sa s 3 životmi
            bonusLiveGiven = false;                                 //bunusovy život ešte neudelený
            map = new Map(this, pathPlan, pathIcons, pathBonus);    //vytvorenie mapy pre 1 level
            map.Print(g);                                           //vykreslenie mapy
            if (gameStatus== GameStatus.preparation)
                gameStatus = GameStatus.ready;                      //pokiaľ sa nenarazilo na chybu, hra je pripravená
        }
        /// <summary>
        ///zmení stav hry na zadaný stav 
        /// </summary>
        /// <param name="newGameStatus"></param>
        public void SetGameStatus(GameStatus newGameStatus)
        {
            this.gameStatus = newGameStatus;
        }
        /// <summary> 
        ///vracia aktuálny stav hry
        /// </summary>
        /// <returns></returns>
        public GameStatus GetGameStatus()
        {
            return gameStatus;
        }
        /// <summary>
        ///nastavý chybovú spravu na argument a zmený stav hry na chybový 
        /// </summary>
        /// <param name="err"></param>
        public void SetErrMessage(string err)
        {
            errMessage = err;
            gameStatus = GameStatus.error;
        }
        /// <summary> 
        ///vráti informáciu o chybe
        /// </summary>
        /// <returns></returns>
        public string GetErrMessage()
        {
            return errMessage;
        }
        /// <summary> 
        ///vrati aktuálny počet životov
        /// </summary>
        /// <returns></returns>
        public int Lives()
        {
            return lives;
        }
        /// <summary> 
        ///vráti aktuálne celkové skóre
        /// </summary>
        /// <returns></returns>
        public int TotalScore()
        {
            return earnedScore+map.Score();
        }
        /// <summary>
        /// vrati číslo levelu
        /// </summary>
        /// <returns></returns>
        public int Lvl()
        {
            return lvl;
        }

        /// <summary>
        /// organizuje priebeh ťahu
        /// </summary>
        /// <param name="keyPressed"></param>
        public void MakeTurn(KeyPressed keyPressed)
        {
            map.MoveObjects(keyPressed);                            // pohyb prvkov na aktuálnej mape                    
            map.Print(g);                                           // vykreslenie mapy
            if ((!bonusLiveGiven) && ((map.Score() + earnedScore) > 10000))
            {
                lives++;                                            //pokiaľ bola dosiahnutá požadovaná hranica bodov pridá bonusový život
                bonusLiveGiven = true;
            }    
                
        }

        /// <summary>
        /// načitanie novej mapy
        /// </summary>
        public void  NextLevel()
        {
            earnedScore += map.Score();                             //pripočíta zisk bodov z dokončeného levelu k získanim bodom
            lvl++;                                                  //postup do dalšieho levelu
            map = new Map(this, pathPlan, pathIcons, pathBonus);    //načítanie novej mapy
            map.Print(g);                                           //vykreslenie mapy
            gameStatus = GameStatus.ready;                          //hra je pripravená
        }

        /// <summary>
        /// koroluje či sa v hre ešte pokračuje a prichystá mapu na pokračovanie
        /// </summary>
        public void Respawn()
        {
            lives--;                                                //strata života
            if (lives > 0)
            {
                map.Reset();                                        //navratenie pohyblivých ojdektov do východiskovej polohy
                map.Print(g);                                       //vykreslenie mapy
                gameStatus = GameStatus.ready;                      //hra je pripravená
            }
            else
                gameStatus = GameStatus.gameover;                   //hra skončila hráč nemá viac životov
        }
    }
}
