﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    /// <summary>
    /// určuje stavy hry
    /// </summary>
    enum GameStatus
    {
        preparation,
        ready,
        playing,
        victory,
        defeat,
        gameover,
        error
    }
}