﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    class RedGhost : Ghost
    {
        public RedGhost(Game game, Map map, int x, int y, int direction)
            : base(game, map, x, y, direction)
        {
            scaterx = 18;
            scatery = 20;
        }
        /// <summary>
        /// prenasleduje pacmana po najkratšej ceste
        /// </summary>
        public override void Chase()
        {
            int newx = 0;
            int newy = 0;
            int x = 0;
            map.PacManPozition(ref newx, ref newy,ref x);           //zistí polohu pacmana
            Bfs(ref newx, ref newy);                                //zisti políčko na ktoré sa má posunúť aby sa dostal k pacmanovi
            Relocate( newx, newy);
        }
        /// <summary>
        /// uteká do ľavého dolného rohu
        /// </summary>
        public override void Scater()
        {
            int newx = scaterx;
            int newy = scatery;
            Bfs(ref newx, ref newy);
            Relocate(newx, newy);
        }
        /// <summary>
        /// pohybuje sa náhodne
        /// </summary>
        public override void Frightened()
        {
            int newx = 0;
            int newy = 0;
            Random(ref newx, ref newy);
            Relocate(newx, newy);
        } 
    }
}
