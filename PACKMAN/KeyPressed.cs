﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    /// <summary>
    /// možnosti stlačených kláves
    /// </summary>
    enum KeyPressed {
        none,
        left,
        up,
        right,
        down
    }
}