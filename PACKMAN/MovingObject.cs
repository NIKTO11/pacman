﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PACMAN
{
    /// <summary>
    /// obsahuje základne premenné a funkcie ktoré sú potrebné pre pohybujúce sa objekty na mape
    /// </summary>
    abstract class MovingObject
    {
        protected Game game;                            //v ktorej hre objekt existuje
        protected Map map;                              //na ktorej mape sa nachádza
        protected int x;                                    
        protected int y;                                //aká je jeho poloha 
        protected int defaultx;
        protected int defaulty;                         //z´akej polohy štartuje
        protected int direction;                        //ktorým smerom sa pozerá
        protected char block;                           //aký objekt sa nachádza pod ním
        public MovingObject(Game game, Map map, int x, int y, int direction)
        {
            this.game = game;
            this.map = map;
            this.x = x;
            this.y = y;
            this.defaultx = x;
            this.defaulty = y;
            this.direction = direction;
            this.block = '_';                           //na začiatku je každý pohyblivý objekt na prázdnom políčku
        }

        public abstract void Move();                    //každy pohyblivý objek sa musí vedieť pohnuť
        public abstract int GetDirection();             //každy pohablivý objekt musí vedieť povedať svoj smer
        /// <summary>
        /// pokiaľ je to možné vráti objekt na jeho pôvodné miesto a otočí ho smerom nahor
        /// </summary>
        public void Reset()
        {
            if (map.Free(defaultx, defaulty))
            {
                map.Relocate(x, y, defaultx, defaulty, ref block);
                this.x = defaultx;
                this.y = defaulty;
            }
            direction = 0;
        }
    }
}
