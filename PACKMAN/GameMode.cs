﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{   
    /// <summary>
    /// určuje správanie duchov
    /// </summary>
    enum GameMode
    {
        chasing,           
        scatering,
        frightened
    }
}