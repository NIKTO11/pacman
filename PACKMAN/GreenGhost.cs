﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    class GreenGhost : Ghost
    {
        public GreenGhost(Game game, Map map, int x, int y, int direction)
            : base(game, map, x, y, direction)
        {
            scaterx = 18;
            scatery = 2;
        }
        /// <summary>
        /// pohybuje sa náhodne 
        /// </summary>
        public override void Chase()
        {
            int newx = 0;
            int newy = 0;
            Random(ref newx, ref newy);                                     // náhnodne vyberie smer pohybu
            Relocate(newx, newy);
        }
        /// <summary>
        /// uteká do pravého horného rohu
        /// </summary>
        public override void Scater()
        {
            int newx = scaterx;
            int newy = scatery;
            Bfs(ref newx, ref newy);                                        //hľadanie najkratšej cesty do pravého horného rohu
            Relocate(newx, newy);
        }
        /// <summary>
        /// pohybuje sa náhodne
        /// </summary>
        public override void Frightened()
        {
            int newx = 0;
            int newy = 0;
            Random(ref newx, ref newy);
            Relocate(newx, newy);
        }
    }
}
