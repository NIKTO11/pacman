﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PACMAN
{
    public partial class Form1 : Form
    {
        Graphics g;                                                 
        Game game;                                                  //aktuálne hraná hra
        int countdown;                                              //dlžka odpočtu
        KeyPressed keyPressed = KeyPressed.none;                    //aktualne stlačená klávesa

        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// zvíditelnenie a skrytie ojektov pre hru a menu
        /// </summary>
        /// <param name="b"></param>
        private void SetToGameMode(bool b)
        {
            labelName.Visible = !b;
            startButton.Visible = !b;
            exitButton.Visible = !b;
            labelScore.Visible = b;
            labelScoreValue.Visible = b;
            labelLives.Visible = b;
            labelLivesValue.Visible = b;
            labelLevel.Visible = b;
            labelLevelValue.Visible = b;
        }

        //po stlačení spusti novú hru
        private void button1_Click(object sender, EventArgs e)
        {
            this.SetToGameMode(true);                               //skyje menu a zobrazi hracie položky
            Size = new Size(1000,1000);                             //zváčší okno
            g = CreateGraphics();
            game = new Game("Game.txt",g);                          //vytvorí novú hru
            labelLivesValue.Text = game.Lives().ToString();
            labelScoreValue.Text = game.TotalScore().ToString();    
            labelLevelValue.Text = game.Lvl().ToString();
            timer1.Enabled = true;                                  //zapne timer hry
        }

        //ukončí program
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //na základe stavu hry zavola príslušnú obsluhu situácie
        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (game.GetGameStatus())                           //na základe stavu hry zvolí postup
            {
                case GameStatus.ready:                              //hra je pripravená
                    countdown= 3;                                   //nastavenie počtu sekund odpočtu
                    labelCountdown.Visible= true;
                    timer1.Enabled = false;                         //pozastavenie hry
                    timer2.Enabled = true;                          //spustenie odpočtu
                    break;
                case GameStatus.playing:                            //hrá sa
                    game.MakeTurn(keyPressed);                      //zabezpečí zavolanie pohybu prvkov
                    labelScoreValue.Text = game.TotalScore().ToString();    //aktualizovanie skóre
                    labelLivesValue.Text = game.Lives().ToString(); //aktualizovanie životov
                    keyPressed = KeyPressed.none;                   //vynulovanie stlačenej klávesy
                    break;
                case GameStatus.victory:
                    timer1.Enabled = false;                         //zastavenie hry
                    game.NextLevel();                               //vytvorenie noveho levelu
                    labelLevelValue.Text = game.Lvl().ToString();   //aktualizácia levelu
                    timer1.Enabled = true;                          //spustenie hry
                    break;
                case GameStatus.defeat:                         
                    timer1.Enabled = false;                         //zastavenie hry
                    game.Respawn();                                 //príprava na pokračovanie
                    labelLivesValue.Text = game.Lives().ToString(); //aktualizácia životov
                    timer1.Enabled = true;                          //spustenie hry
                    break;
                case GameStatus.gameover:
                    timer1.Enabled = false;                         //ukončenie hry    
                    MessageBox.Show("Game Over");                   //oboznámenie užívateľa o ukončení
                    this.SetToGameMode(false);                      //návrat do menu
                    g.DrawImage(new Bitmap( "black.png"), 0, 0);    //nastavenie pozadia
                    Size = new Size(1000, 500);                     //uprava veľkosti
                    break;
                case GameStatus.error:                              //nastala chyba
                    timer1.Enabled = false;                         //ukončenie hry
                    MessageBox.Show(game.GetErrMessage());          //oboznámenie o chybe
                    this.Close();                                   //ukončenie programu
                    break;
                default:
                    break;
            }
        }

        //odpočitava začiatok hry po jej prerušení
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (countdown > 0)                                      
            {
                labelCountdown.Text = countdown.ToString();         //pokiaľ zostáva viac ako 0 do konca odpočtu, aktualizuj odpočet
            }
            else if (countdown == 0)
            {
                labelCountdown.Text = "GO!";                        //ak je odpočet na 0 zobraz "GO!"
            }
            else                                                    //odpočet je menej ako 0
            {
                game.SetGameStatus( GameStatus.playing);            //hrá sa
                labelCountdown.Visible = false;                     //skrytie odpočtu
                timer2.Enabled = false;                             //vypnutie odpočtu
                timer1.Enabled = true;                              //spustenie hry
            }
            countdown--;                                            //uplinula 1 sekunda z odpočtu
        }

        //zachytávanie stlačenia kláves šípok
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Up)
            {
                keyPressed = KeyPressed.up;
                return true;
            }
            if (keyData == Keys.Down)
            {
                keyPressed = KeyPressed.down;
                return true;
            }
            if (keyData == Keys.Left)
            {
                keyPressed = KeyPressed.left;
                return true;
            }
            if (keyData == Keys.Right)
            {
                keyPressed = KeyPressed.right;
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
