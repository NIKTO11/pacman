﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PACMAN
{
    /// <summary>
    /// obsahuje informácie o mape a prvkoch na nej a pomáha koordinovať ich vzájomnú interakciu.
    /// Zabezpečuje tiež zavolanie ťahu pre všetky prvky a vykreslovanie aktuálneho stavu na obrazovku
    /// </summary>
    class Map
    {
        Game game;                                                  //hra v ktorej mapa existuje

        int width;                                                  //šírka mapy
        int height;                                                 //výška mapy
        int iconSize;                                               //veľkosť jedného bloku pri vykreslovaní
        char[,] plan;                                               //aktuálna mapa
        Bitmap[] icons;                                             //ikony predmetov 
        Bitmap[] bonus;                                             //ikony bonusu
        GameMode gameMode;                                          //aktuálny mod hry
        int modeDuration;                                           //dĺžka aktualneho módu v desatinách sekundy
        int speed;                                                  //ako často duchovia neťahaju

        PacMan pacMan;                                              
        RedGhost redGhost;
        PinkGhost pinkGhost;
        BlueGhost blueGhost;
        GreenGhost greenGhost;

        int score;                                                  //score dosiahnuté na mape
        bool bonusSpawn = true;                                     //či sa už bonus pridal
        int pelletCount = 0;                                        //koľko bodiek je na mape
        int bonusx;                                                 //x suradnica bonusu
        int bonusy;                                                 //y súradnica bonusu
        int killScore;                                              //kolko bodov treba prideliť za zjedenie ducha

        public Map(Game game, string pathPlan, string pathIcon, string pathBonus)
        {
            this.game = game;
            LoadIcons(pathIcon, ref icons);                         //načitanie ikon predmetov
            LoadIcons(pathBonus, ref bonus);                        //načítanie ikon bonusu
            LoadPlan(pathPlan);                                     //načítanie mapy
            gameMode = GameMode.scatering;                          
            modeDuration = 0;
            speed = 4;
        }

        /// <summary>
        /// načita do images postupne štvorcové bitmapy z obrázku zadaného v path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="images"></param>
        private void LoadIcons(string path,ref Bitmap[] images)
        {
            Bitmap bitmap = new Bitmap(path);                       //načítanie obrázka
            iconSize = bitmap.Height;                               
            int count = bitmap.Width / iconSize;                    //kolko ikon treba načitať
            images = new Bitmap[count];
            for (int i = 0; i < count; i++)
            {
                Rectangle rectangle = new Rectangle(i * iconSize, 0, iconSize, iconSize);            //načítanie obrázka
                images[i] = bitmap.Clone(rectangle, System.Drawing.Imaging.PixelFormat.DontCare);    //priradenie obrázka do premennnej
            }
        }

        /// <summary>
        /// z dokumentu nachadzajúceho sa v path načíta mapu do plan
        /// </summary>
        /// <param name="path"></param>
        private void LoadPlan(string path)
        {
            bool pacmanFound = false;

            System.IO.StreamReader planFile = new System.IO.StreamReader(path);
            width = int.Parse(planFile.ReadLine());                 //prvý riadok obsahuje šírku
            height = int.Parse(planFile.ReadLine());                //druhý riadok výšku
            plan = new char[width, height];                         //vytvorenie pola pre mapu

            for (int y = 0; y < height; y++)
            {
                string line = planFile.ReadLine();
                for (int x = 0; x < width; x++)
                {
                    char c = line[x];
                    plan[x, y] = c;                                 //uloženie znaku do pola na prislušnú súradnicu

                    switch (c)
                    {
                        case 'C':                                   //našiel sa pacman
                            if (pacmanFound)
                            {
                                game.SetErrMessage( "Too many PacMans");    //nieje to prvý pacman
                            }
                            else
                            {
                                pacMan = new PacMan(game,this, x, y, 0);    //vytvorenie pacmana
                                pacmanFound = true;
                            }
                            break;
                        case 'R':                                           //najdenie červeného ducha
                            redGhost = new RedGhost(game, this, x, y, 0);   //vytvorenie červeného ducha
                            break;
                        case 'G':                                           //najdenie zeleného ducha    
                            greenGhost = new GreenGhost(game, this, x, y, 0);   //vytvorenie zeleného ducha
                            break;
                        case 'B':                                           //najdenie modrého ducha
                            blueGhost = new BlueGhost(game, this, x, y, 0); //vytvorenie modrého ducha
                            break;
                        case 'P':                                           //najdenie ružového ducha
                            pinkGhost = new PinkGhost(game, this, x, y, 0); //vytvorenie ružového ducha
                            break;
                        case '.':                                   
                        case 'O':                                                  
                            pelletCount++;                          //najdená potrava, zvýšenie počtu ktorý treba zozbierať
                            break;
                        default:
                            break;
                    }
                }
            }
            bonusx = int.Parse(planFile.ReadLine());                //x,y súradnica bonusu, kde sa má objaviť
            bonusy = int.Parse(planFile.ReadLine());

        }

        /// <summary>
        /// vykreslí na obrazovku hraciu plochu v aktuálnom stave
        /// </summary>
        /// <param name="g"></param>
        public void Print(Graphics g)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    char c = plan[x, y];
                    if (c != '+')                                   // pokiaľ nejde o bonus
                    {
                        int index = "TJL7-I*_.OCxxxxRxxxxGxxxxPxxxxBxxxx".IndexOf(c);   //poradie ikon icons a ich písmenková reprezentácia v plan, x sú jednotlivé otočenia pre predchádzajúci prvok
                        switch (c)                                  //zistuje ako je natočený objekt aby ho správne vykreslil
                        {
                            case 'C':
                                index += pacMan.GetDirection();     
                                break;
                            case 'R':
                                index += redGhost.GetDirection();
                                break;
                            case 'G':
                                index += greenGhost.GetDirection();
                                break;
                            case 'B':
                                index += blueGhost.GetDirection();
                                break;
                            case 'P':
                                index += pinkGhost.GetDirection();
                                break;
                            default:
                                break;

                        }
                        g.DrawImage(icons[index], x * iconSize, y * iconSize);  //vykreslenie na obrazovku
                    }
                    else
                    {
                        if ((game.Lvl()) <= 10)
                            g.DrawImage(bonus[game.Lvl() - 1], x * iconSize, y * iconSize);     //zvolenie ikonky bonusu
                        else
                            g.DrawImage(bonus[10], x * iconSize, y * iconSize);
                    }
                }
            }
        }

        /// <summary>
        /// vráti true ak je políčko x y prázdne
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Empty(int x, int y)
        {
            return (plan[x, y] == '_');
        }
        /// <summary>
        /// vrati true ak je na políčku x y potrava
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Pellet(int x, int y)
        {
            return (plan[x, y] == '.');
        }
        /// <summary>
        /// vráti true ak je na políčku x y zosilujúca potrava
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool PowerPellet(int x, int y)
        {
            return (plan[x, y] == 'O');
        }
        /// <summary>
        /// vráti true ak je  políčko x y stena
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Wall(int x, int y)
        {
            return (plan[x, y] == 'I' || plan[x, y] == '-' || plan[x, y] == 'L' || plan[x, y] == 'J' || plan[x, y] == 'T' || plan[x, y] == '7'); 
        }
        /// <summary>
        /// vráti true ak je políčko x y brána
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Gate(int x, int y)
        {
            return (plan[x, y] == '*');
        }
        /// <summary>
        /// vráti true ak je na políčku x y duch
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Ghost(int x, int y)
        {
            return (plan[x, y] == 'P' || plan[x, y] == 'B' || plan[x, y] == 'G' || plan[x, y] == 'R');
        }
        /// <summary>
        /// vráti true ak je na políčku x y pacman
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool PacMan(int x, int y)
        {
            return (plan[x, y] == 'C');
        }
        /// <summary>
        /// vráti true ak je na políčku x y bonus
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool BonusItem(int x, int y)
        {
            return (plan[x, y] == '+');
        }
        /// <summary>
        /// vráti true ak sa na políčko x y dá presunúť(je prázdne alebo s jednom)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Free(int x, int y)
        {
            if (Empty(x, y) || Pellet(x, y) || PowerPellet(x, y) || BonusItem(x, y))
                return (true);
            else
                return (false);
        }

        /// <summary>
        /// zobere objekt z x y a nahradí ho s block, do block dosadí objekt z xx yy a na xx yy umiesti povôdný objekt z x y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        /// <param name="block"></param>
        public void Relocate(int x, int y, int xx, int yy, ref char block)
        {
            char c = plan[xx, yy];                              //uloží si obsah xx yy
            plan[xx, yy] = plan[x, y];                          //premiestni x y na xx yy
            plan[x, y] = block;                                 //umiestni na x y block
            block = c;                                          //do bloku dosadí povodný obsah xx yy
        }
        /// <summary>
        /// vráti aktuálny mód hry
        /// </summary>
        /// <returns></returns>
        public GameMode GetGameMode()
        {
            return gameMode;
        }
        /// <summary>
        /// skontroluje či je x súradnica v medziach poľa a ak nie tak ju príslušne napraví
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public int Check(int x)
        {
            if (x < 0)                                          //súradnica vychádza z poľa naľavo
            {
                return (x + width);                             //presun na druhú stranu hracej pochy
            }
            else
            {
                if (x >= width)                                 //súradnica vychádza z poľa napravo
                    return (x - width);                         //presun na druhú stranu hracej pochy
                else
                    return (x);                                 //súradnica sa nachádza v poli
            }
        }
        /// <summary>
        /// vráti hodnotu score dosiahnutého na aktuálnej mape
        /// </summary>
        /// <returns></returns>
        public int Score()                                       
        {
            return (score);
        }

        /// <summary>
        /// odoberie potravu a pridá príslušné množstvo bodov 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void ItemCollected(int x, int y)
        {
            switch (plan[x, y])
            {
                case '.':
                    score += 10;                                //zisk 10 bodov
                    pelletCount--;                              //zníženie počtu zostávajúcej potravy
                    break;
                case '+':
                    score += 500 * game.Lvl();                  //body za bonus
                    break;
                case 'O':                                       //zníženie počtu zostávajúcej potravy
                    pelletCount--;                              
                    gameMode = GameMode.frightened;             //prepnutie do módu požierania duchov
                    if (game.Lvl() > 9)
                        modeDuration = 10;
                    else if (game.Lvl() > 3)
                        modeDuration = 30;
                    else
                        modeDuration = 50;                      //nastavenie dĺžky modu v desadinách sekund v závysloti od levelu
                    redGhost.Retreat();                         
                    pinkGhost.Retreat();
                    blueGhost.Retreat();
                    greenGhost.Retreat();                       //zmena správania duchov
                    killScore = 200;                            //reštartovanie bodov za zjedenie ducha
                    speed = 2;                                  //zníženie rýchlosti duchov
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// vráti prázdne pole o rozmeroch hracej pochy
        /// </summary>
        /// <returns></returns>
        public int[,] CreateTempPlan()
        {
            int[,] tempPlan = new int[width, height];
            return tempPlan;
        }
        /// <summary>
        /// zisti pozíciu a smer pacmana a dosadí ich do prarametrov px py a pdirection
        /// </summary>
        /// <param name="px"></param>
        /// <param name="py"></param>
        /// <param name="pdirection"></param>
        public void PacManPozition(ref int px, ref int py, ref int pdirection)
        {
            pacMan.Pozition(ref px, ref py, ref pdirection);
        }
        /// <summary>
        /// predá pacmanovi stlačenú klavesu a  požiada všetky prvky o pohyb
        /// </summary>
        /// <param name="keyPressed"></param>
        public void MoveObjects(KeyPressed keyPressed)
        {

            pacMan.SetKeyPressed(keyPressed);                   //predanie stlačenej klavesy
            pacMan.Move();                                      //pohyb pacmana
            if (modeDuration % speed != 0)                      //ak sú duchovia na ťahu, pohni s nimi
            {
                redGhost.Move();                                
                pinkGhost.Move();
                blueGhost.Move();
                greenGhost.Move();
            }

            if (score >= 1000 && bonusSpawn && Empty(bonusx, bonusy))   //ak bol dosiahnutý dostatočný počet bodov, otvor bonus
            {
                plan[bonusx, bonusy] = '+';
                bonusSpawn = false;
            }

            if (pelletCount == 0)                               //ak sú zozbierané všetky potravy, mapa je vyhratá
            {
                game.SetGameStatus( GameStatus.victory);
            }
            modeDuration--;
            if (modeDuration < 0)                               //pokiaľ vypršal čas pre aktuálny mód
            {
                switch (gameMode)
                {
                    case GameMode.chasing:
                        gameMode = GameMode.scatering;          //prepni do modu scatering
                        modeDuration = 50;                      //nastavenie dlžky na 5s
                        break;
                    case GameMode.scatering:
                    case GameMode.frightened:
                        gameMode = GameMode.chasing;            //prepni do modu chasing
                        modeDuration = 200 + game.Lvl() * 50;   //na 20 sekund plus 5 s za level
                        speed = 4;                              //vráť rýchlosť na pôvodnú
                        break;
                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// zabi ducha na pozící x y a znovuzroď ho na štartovacej pozící
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void KillGhost(int x, int y)
        {
            bool succesfullKill=false;                          //uspešnosť zabitia
            switch (plan[x,y])                                  //zabitie príslušného ducha
            {
                case 'R':
                    succesfullKill = redGhost.Kill();
                    break;
                case 'G':
                    succesfullKill = greenGhost.Kill();
                    break;
                case 'B':
                    succesfullKill = blueGhost.Kill();
                    break;
                case 'P':
                    succesfullKill = pinkGhost.Kill();
                    break;
                default:
                    break;
            }
            if (succesfullKill)                                 //ak bol duch zabitý, pripočítaj skore a zdvojnásob odmenu za dalšieho
            {
                score += killScore;
                killScore *= 2;
            }
        }
        /// <summary>
        /// premiestni duchov a pacmana na ich vychodiskové pozície
        /// </summary>
        public void Reset()
        {
            for (int i = 0; i < 5; i++)                         //pre prípad že by si navzájom stáli na svojich štartovacích pozíciach, vždy sa premiestni aspoň jeden
            {
            pacMan.Reset();
            redGhost.Reset();
            pinkGhost.Reset();
            blueGhost.Reset();
            greenGhost.Reset();
            }
            if (gameMode == GameMode.frightened)
            {
                gameMode = GameMode.scatering;                  //ak počas frightened modu pacman zomrie, do jeho konca má pacman scatering mode  
            }
        }
    }
}