﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    /// <summary>
    /// obsahuje základne premenné a funkcie ktoré sú potrebné pre duchov
    /// </summary>
    abstract class Ghost : MovingObject
    {
        protected int scaterx;
        protected int scatery;                                          //súradnice pre scater správanie
        Random random = new Random();                                   
        int obstacle =-1;                                               //hodnota neprístupného políčka
        protected bool died = false;                                    //informácia o umrti od zjedenia posledného powerpelletu 

        public Ghost(Game game, Map map, int x, int y, int direction)
            : base(game, map, x, y, direction)
        {
        }

        public abstract void Chase();                                   //funcia pre správanie v chase móde
        public abstract void Scater();                                  //funcia pre správanie v scater móde
        public abstract void Frightened();                              //funcia pre správanie v frightened móde
        /// <summary>
        /// vráti svoj smer alebo 4 keď je v frightened mode
        /// </summary>
        /// <returns></returns>
        public override int GetDirection()
        {
            if (map.GetGameMode() == GameMode.frightened&&!died)
                return 4;
            else
                return this.direction;
        }
        /// <summary>
        /// zavolá funkciu správneho pohybu na základe aktuálneho módu hry
        /// </summary>
        public override void Move()
        {
            switch (map.GetGameMode())
            {
                case GameMode.chasing:
                    Chase();
                    break;
                case GameMode.scatering:
                    Scater();
                    break;
                case GameMode.frightened:
                    if (died)
                        Chase();
                    else
                        Frightened();
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// premiestni ducha na pozíciu xx yy ak je to možné
        /// </summary>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        public void Relocate(int xx, int yy)
        {
            xx = map.Check(xx);                                 //skontroluje súrádnicu x
            CheckDirection(ref xx,ref yy);                      //skontroluje či je duch správne natočený
            if (map.Free(xx, yy) || map.Gate(xx, yy))           //aj ke voľne presunie sa na pozíciu xx yy
            {
                map.Relocate(x, y, xx, yy, ref block);
                this.x = xx;
                this.y = yy;
            }
            else if (map.PacMan(xx, yy))                        //ak narazí na pacmana
            {
                if (map.GetGameMode() != GameMode.frightened||died)     
                    game.SetGameStatus(GameStatus.defeat);      //pokiaľ nieje frightened, zabije pacmana

            }
        }
        /// <summary>
        /// kontruluje či políčko na ktoré sa duch chce posunúť je v smere jeho pohľadu
        /// </summary>
        /// <param name="newx"></param>
        /// <param name="newy"></param>
        public void CheckDirection(ref int newx, ref int newy)
        {
            int movex = newx - x;
            int movey = newy - y;
            bool rightDirection = false;
            if ((movex == 0) && (movey == -1) && (direction != 0))
            {
                direction = 0;
            }
            else if ((movex == 1) && (movey == 0) && (direction != 1))
            {
                direction = 1;
            }
            else if ((movex == 0) && (movey == 1) && (direction != 2))
            {
                direction = 2;
            }
            else if ((movex == -1) && (movey == 0) && (direction != 3))
            {
                direction = 3;
            }
            else
            {
                rightDirection = true;
            }

            if (!rightDirection)
            {
                newx = x;
                newy = y;
            }
        }
        /// <summary>
        /// ak sa na pozíciu xx yy dá posunúť pridá ju do queue a do poľa tempPlan pridá hodnotu i+1 inak hodnotu obstacle
        /// </summary>
        /// <param name="tempPlan"></param>
        /// <param name="i"></param>
        /// <param name="queue"></param>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        private void AddToQueue(ref int[,] tempPlan, int i, ref Queue<int> queue, int xx, int yy)
        {
            xx = map.Check(xx);                                 //skontroluje súrádnicu x
            if (tempPlan[xx, yy] == 0)                          //pokiaľ sa na políčko ešte nikto nepýtal
            {
                if (map.Wall(xx,yy))                            //ak je tam stena je to prekážka
                {
                    tempPlan[xx, yy] = obstacle;
                }
                else
                {
                    queue.Enqueue(xx);
                    queue.Enqueue(yy);
                    tempPlan[xx, yy] = i + 1;                   //inak pridaj xx yy do fronty a dosať na túto pozíciu i
                }
            }
        }
        /// <summary>
        /// hladá najkratšiu cestu na pozíciu newx newy z aktuálnej pozície prehľadávaním do šírky a za newx newy dosadí súradnice najblížšieho políčka na najkratšej ceste
        /// </summary>
        /// <param name="newx"></param>
        /// <param name="newy"></param>
        public void Bfs(ref int newx, ref int newy)
        {
            int xx = x;
            int yy = y;

            Queue<int> queue = new Queue<int>();                //vytvorenie fronty
            int[,] tempPlan = map.CreateTempPlan();             //vytvorěnie poľa na backtracking
            AddToQueue(ref tempPlan, 0, ref queue, xx, yy);     //pridanie prvého prvku do poľa
            //bfs
            while ((newx != xx)||(newy != yy))                  //pokiaľ sa nedostanem na požadovaného políčka
            {
                xx = queue.Dequeue();                                               
                yy = queue.Dequeue();                                               //zober prvok z fronty                              

                int i = tempPlan[xx, yy];                                           
                AddToQueue(ref tempPlan, i, ref queue, xx - 1, yy);             
                AddToQueue(ref tempPlan, i, ref queue, xx, yy - 1);
                AddToQueue(ref tempPlan, i, ref queue, xx + 1, yy);
                AddToQueue(ref tempPlan, i, ref queue, xx, yy + 1);             //pridaj do fronty jeho susedov

            }
            //backtracking
            while (tempPlan[xx, yy] != 1)                                           //pokiaľ niesom vzdialený o menej ako jedna od aktuálnej pozície
            {
                newx = xx;
                newy = yy;
                if (tempPlan[map.Check(xx - 1), yy] > 0 && tempPlan[xx, yy] > tempPlan[map.Check(xx - 1), yy])
                {
                    xx = xx - 1;
                }
                else if (tempPlan[xx, yy - 1] > 0 && tempPlan[xx, yy] > tempPlan[xx, yy - 1])
                {
                    yy = yy - 1;
                }
                else if (tempPlan[map.Check(xx + 1), yy] > 0 && tempPlan[xx, yy] > tempPlan[map.Check(xx + 1), yy])
                {
                    xx = xx + 1;
                }
                else if (tempPlan[xx, yy + 1] > 0 && tempPlan[xx, yy] > tempPlan[xx, yy + 1])
                {
                    yy = yy + 1;
                }
                xx = map.Check(xx);                                                 //posuň sa do smeru kde klesá hodnota políčka ale nieje tam prekážka a skontroluj xx
            }
        }
        /// <summary>
        /// zmení suradnice xx yy na suradnice políčka o jedno vedľa v smere moveDirection
        /// </summary>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        /// <param name="moveDirection"></param>
        public void MoveInDirection(ref int xx, ref int yy,int moveDirection )
        {
            switch (moveDirection)
            {
                case 0:
                    yy--;
                    break;
                case 1:
                    xx++;
                    break;
                case 2:
                    yy++;
                    break;
                case 3:
                    xx--;
                    break;
                default:
                    break;
            }
            xx = map.Check(xx);
        }
        /// <summary>
        /// vrati true pokiaľ  je políčko xx yy v chodbe, teda má dve steny po stranách 
        /// </summary>
        /// <param name="xx"></param>
        /// <param name="yy"></param>
        /// <param name="moveDirection"></param>
        /// <returns></returns>
        public bool Corridor(int xx , int yy, int moveDirection)
        {
            switch (moveDirection)
            {
                case 0:
                case 2:
                    return (map.Wall(map.Check(xx - 1), yy) && map.Wall(map.Check(xx + 1), yy));
                case 1:
                case 3:
                    return (map.Wall(xx, yy - 1) && map.Wall(xx, yy + 1));
                default:
                    return false;
            }
        }
        /// <summary>
        /// do newx newy dosadi náhodne zvolené políčko na ktoré sa može duch aktuálne pohnúť
        /// </summary>
        /// <param name="newx"></param>
        /// <param name="newy"></param>
        public void Random(ref int newx, ref int newy)
        {
            newx = this.x;
            newy = this.y;
            if (Corridor(x, y, direction))                      //ak je v chodbe
            {
                MoveInDirection(ref newx, ref newy, direction); //pohni sa o jedno do predu v sme ktorom sa pohybuješ
                if (!map.Free(newx, newy))                      //pokiaľ sa tam nedá posunúť
                {
                    newx = this.x;
                    newy = this.y;
                    direction = (direction + 2) % 4;            //otočí sa na opačnú stranu
                }
            }
            else                                                //je na križovatke
            {
                int xx = newx;
                int yy = newy;
                while (!(map.Free(xx, yy) || map.Gate(xx, yy) || map.PacMan(xx, yy)))   //poiaľ si nevybere políčko na ktoré môže isť, vyberá si smer
                {
                    xx = newx;
                    yy = newy;
                    int r = random.Next(4);
                    MoveInDirection(ref xx, ref yy, r);
                }
                newx = xx;
                newy = yy;
            }
        }
        /// <summary>
        /// otočí sa do protismeru a môže opäť byť zjedený
        /// </summary>
        public void Retreat()
        {
            direction = (direction + 2) % 4;
            died = false;
        }
        /// <summary>
        /// pokiaľ može byť zjedený, znovuzrodi ho v miestnosti duchov uprostred mapy
        /// </summary>
        /// <returns></returns>
        public bool Kill()
        {
            if (!died)
            {
                died = true;
                int xx = 8;                    
                int yy = 10;
                while (!map.Free(xx, yy)) //hľadá prazdne políčko v miestnoti duchov
                {
                    xx++;
                }
                Relocate(xx, yy);         //premiestnenie na najdené políčko
                this.x = xx;
                this.y = yy;
                return (true);
            }
            else
            {
                return (false);
            }
        }

    }
}
