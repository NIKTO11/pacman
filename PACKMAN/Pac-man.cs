﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACMAN
{
    /// <summary>
    /// obsahuje informácie o pacmanovi a funkcie umožnujúce jeho ovládanie
    /// </summary>
    class PacMan : MovingObject
    {
        public PacMan(Game game, Map map, int x, int y, int direction)
            : base(game, map, x, y, direction)
        {
        }

        KeyPressed keyPressed;                                  //naposledy stlačená klávesa
        /// <summary>
        /// nastavenie stlačenej klávesy
        /// </summary>
        /// <param name="key"></param>
        public void SetKeyPressed(KeyPressed key)
        {
            keyPressed = key;
        }
        /// <summary>
        /// vrati svoj smer
        /// </summary>
        /// <returns></returns>
        public override int GetDirection()
        {
            return this.direction + 1;
        }
        /// <summary>
        /// dosadí do px py svoju polohu a do pdirection svoj smer
        /// </summary>
        /// <param name="px"></param>
        /// <param name="py"></param>
        /// <param name="pdirection"></param>
        public void Pozition(ref int px, ref int py, ref int pdirection)
        {
            px = this.x;
            py = this.y;
            pdirection = this.direction;
        }
        /// <summary>
        /// obsluha pohybu pacmana na základe posledne stlačenej klávesy 
        /// </summary>
        public override void Move()
        {
            int xx = x;
            int yy = y;
            switch (keyPressed)                                 //podľa stlkačenej klávesy upravý suradnice polohy na ktoré sa chce posunúť
            {
                case KeyPressed.none:
                    xx = x;
                    yy = y;
                    break;
                case KeyPressed.up:
                    xx = x;
                    yy = y - 1;
                    direction = 0;
                    break;
                case KeyPressed.right:
                    xx = x + 1;
                    yy = y;
                    direction = 1;
                    break;
                case KeyPressed.down:
                    xx = x;
                    yy = y + 1;
                    direction = 2;
                    break;
                case KeyPressed.left:
                    xx = x - 1;
                    yy = y;
                    direction = 3;
                    break;
                default:
                    break;
            }

            xx = map.Check( xx);                                //kontrola x súradnice
            if (map.Free(xx, yy))                               //ak je možné uskutočniť premiestnenie
            {
                map.ItemCollected(xx,yy);                       //zje potravu
                char c = block;
                map.Relocate(x, y, xx, yy, ref c);              // posunie sa
                this.x = xx;
                this.y = yy;                                    //uloží si svoju novú pozíciu
            }
            else if (map.Ghost(xx,yy))                          //narazil na ducha
            {
                if (map.GetGameMode() == GameMode.frightened)   //ak ho može zjesť, zje ho
                {
                    map.KillGhost(xx, yy);
                    char c = block;
                }
                else
                    game.SetGameStatus(GameStatus.defeat);      //inak zomrel
            }
        }
    }
}
